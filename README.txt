Overview
---------
Support for multiple Teamspeak viewer blocks.
see http://goteamspeak.com for more info.

Requirements
------------
* Drupal 6.0 or later

Author
-------
Kenn Persinger <lestat@iglou.com>

